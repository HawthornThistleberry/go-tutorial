package main

import "fmt"

// adder is a function that creates and returns a function
// the return function is int -> int
// each instance of the created function has its own sum variable
// so each call to adder creates effectively an 'object' that includes both a running tally, and a means of updating it
func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}

func main() {
	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}
}
