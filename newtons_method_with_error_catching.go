package main

import (
	"fmt"
)

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	// We need to convert e to a float64(e) because right now it's a ErrNegativeSqrt instead,
	// which means it has an Error method, and it's currently also carrying a non-nil
	// error value, so Sprintf() is cleverly going to call its Error method (me)
	// when asked to display a string value, thus causing infinite recursion. By converting
	// to a plain float64, we strip away the error value and Error() method; this forces
	// Sprintf() to use the normal String() method for float64s instead.
	return fmt.Sprintf("cannot Sqrt negative number: %v", float64(e))
}

func ReallyClose(x, z float64) bool {
	diff := x - z
	if diff < 0 { diff *= -1.0 }
	if diff * 100000 < x { return true }
	return false
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		return 0, ErrNegativeSqrt(x)
	}
	z := x
	lastz := 0.0;
	for !ReallyClose(z, lastz) {
		lastz = z
		z -= (z * z - x) / (2 * z)
	}
	return z, nil
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}
