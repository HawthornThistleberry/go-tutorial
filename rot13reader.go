package main

import (
	"io"
	"os"
	"strings"
	//"fmt"
)

func rot13(c byte) byte {
	if (c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M') {
		return c+13
	}
	if (c >= 'n' && c <= 'z') || (c >= 'N' && c <= 'Z') {
		return c-13
	}
	return c
}

type rot13Reader struct {
	r io.Reader
}

func (rot *rot13Reader) Read(b []byte) (n int, err error) {
	n,err = rot.r.Read(b)
	for i := 0; i < len(b); i++ {
		b[i] = rot13(b[i])
	}
	return
}


func main() {
	//fmt.Printf("a %c, t %c, A %c, T %c, ! %c\n",rot13('a'), rot13('t'), rot13('A'), rot13('T'), rot13('!'))
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
