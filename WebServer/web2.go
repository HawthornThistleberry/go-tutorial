package main

import (
	"log"
	"net/http"
  "fmt"
)

type String string

func (s String) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request) {
	fmt.Fprint(w, "<html><head><title>String</title></head><body>", s, "</body></html>")
}

type Chatter struct {
    Greeting string
    Punct    string
    Who      string
}

func (s *Chatter) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request) {
	fmt.Fprint(w, "<html><head><title>Greeting</title></head><body>", s.Who, " says", s.Punct, " '", s.Greeting, "'</body></html>")
}

func main() {
  http.Handle("/string", String("I'm a frayed knot."))
  http.Handle("/struct", &Chatter{"Hello", ":", "A gopher"})
	log.Fatal(http.ListenAndServe("localhost:4000", nil))
}
