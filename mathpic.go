package main

import (
	"code.google.com/p/go-tour/pic"
    //"math"
)

func Pic(dx, dy int) [][]uint8 {
	r := make([][]uint8, dy)
	for i := 0; i < dy; i++ {
		r[i] = make([]uint8, dx)
	}
	for i, row := range r {
		for j := range row {
			row[j] = uint8( (i+j)/2 )
			//row[j] = uint8(i*j)
			//row[j] = uint8(math.Pow(float64(i)/5.0,float64(j)/20.0))
		}
	}
	return r
}

func main() {
	pic.Show(Pic)
}
