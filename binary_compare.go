package main

import (
  "golang.org/x/tour/tree"
  "fmt"
  )

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
  if t == nil {
    return
  }
  Walk(t.Left, ch)
  ch <- t.Value
  Walk(t.Right, ch)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
// Assumes trees have same number of values.
func Same(t1, t2 *tree.Tree) bool {
  // make buffered channels
  c1 := make(chan int, 10)
  c2 := make(chan int, 10)
  
  // asynchronously walk both trees into their channels
  go Walk(t1, c1)
  go close(c1)
  go Walk(t2, c2)
  go close(c2)
  
  // compare the results as they come back; stop once you find a mismatch
  for i := range c1 {
    if i != <-c2 {
      return false
    }
  }
  return true
}

func main() {
  // test the Walk function by walking a tree to print it
  ch := make(chan int, 10)
  go Walk(tree.New(2), ch)
  go close(ch)
  for i := range ch {
    fmt.Println(i)
  }
  
  // test the asynchronous crawl of Same using two Walks
  fmt.Println(Same(tree.New(1),tree.New(1)), " should be true")
  fmt.Println(Same(tree.New(2),tree.New(3)), " should be false")
}
