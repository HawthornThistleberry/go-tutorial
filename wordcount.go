package main

import (
	"code.google.com/p/go-tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	
	r := make(map[string]int)
	strings := strings.Fields(s)
	
	for _,v := range strings {
		r[v]++;
	}
	return r;
	
	//return map[string]int{"x": 1}
}

func main() {
	wc.Test(WordCount)
}
