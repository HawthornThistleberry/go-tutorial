package main

// http://tour.golang.org/methods/10
// No change in the code, but I added comments as I figured it out to explain it to myself

import (
	"fmt"
	"io"
	"strings"
)

func main() {
	// create a string
	r := strings.NewReader("Hello, Reader!")

	// create a buffer that can hold eight-byte chunks
	b := make([]byte, 8)
	
	
	for {
		// use the provided Read method to peel off eight bytes
		// r. tells it what string to read, Read tells it what to do,
		// and the Read method knows to peel off as much as fit in the 
		// destination provided (b) but no more; this is an attribute
		// of how the string Read method was implemented.
		n, err := r.Read(b)
		// One thing this code doesn't tell me is if Read is actually
		// consuming the contents -- that is, does r no longer have the
		// first eight characters in it? -- or if it's simply keeping
		// track internally of how far it's read, so it knows where to
		// resume from.
		
		fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
		fmt.Printf("b[:n] = %q\n", b[:n])
		if err == io.EOF {
			break
		}
	}
}
