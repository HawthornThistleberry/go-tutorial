package main

import (
	"fmt"
)

func SqrtFixedIterations(x float64) float64 {
	z := x
	i := 0;
	for ; i < 10; i++ {
		z -= (z * z - x) / (2 * z)
	}
	return z
}

func ReallyClose(x, z float64) bool {
	diff := x - z
	if diff < 0 { diff *= -1.0 }
	if diff * 100000 < x { return true }
	return false
}

func Sqrt(x float64) float64 {
	z := x
	lastz := 0.0;
	i := 0
	for !ReallyClose(z, lastz) {
		i++
		lastz = z
		z -= (z * z - x) / (2 * z)
	}
	fmt.Println("calc for", x, "took", i, "iterations")
	return z
}


func main() {
	fmt.Println(Sqrt(2), Sqrt(3), Sqrt(4), Sqrt(5))
}
