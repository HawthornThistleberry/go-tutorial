package main

import "golang.org/x/tour/reader"

// http://tour.golang.org/methods/11

type MyReader struct {}

// TODO: Add a Read([]byte) (int, error) method to MyReader.
func (v MyReader) Read(b []byte) (int, error)  {
	b[0] = 'A'
	return 1, nil
}

func main() {
	reader.Validate(MyReader{})
}
